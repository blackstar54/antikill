package me.bs54.antikill;
import me.bs54.antikill.events.CommandHandler;
import org.bukkit.plugin.java.JavaPlugin;

public class AntiKill extends JavaPlugin {
    @Override
    public void onEnable() {
        getLogger().info("\n\n\n[AntiKill] Started!\nCreated by Lewis/blackstar54 to prevent malice towards me :)\n\n\n");
        getServer().getPluginManager().registerEvents(new CommandHandler(), this);
    }
}
