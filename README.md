# Anti Kill

This is used to prevent any player (oped or not) from running a /kill, /deop, /kick, or /ban on a given username. (full bypass protection included)

## Getting Started

Download intelij and build the artifact. Then put the artifact (antikill.jar) in the plugins folder. (built for 1.14.4)
You can find a compiled version upon release. (or just ask me on Discord if I forget: blackstar54#0012)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Shprqness (for helping me get started with Java)
